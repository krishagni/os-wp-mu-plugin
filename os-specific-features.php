<?php
/*
Plugin Name: OpenSpecimen Specific Features
Version: 1.0.0
Author: Sheeba Abraham
Author URI: http://isheeba.com
License: GPL2
Description: Plugin for Overriding Pluggable functions and adding specific featured for the site. Specifically created for OpenSpecimen.org. E.g Skip email notification for each password changes, send email to admin when a download link is clicked.
More on why MU - Must Use : https://codex.wordpress.org/Pluggable_Functions
MOre on Mu-Plugin : https://gregrickaby.com/2013/10/create-mu-plugin-for-wordpress/
*/


/* Do not send password change notification */
if ( ! function_exists( 'wp_password_change_notification' ) ) {
	function wp_password_change_notification( $user ) {
		return;
	}
}

//Do not send the WP standard email notification on new user creation to user.
if ( ! function_exists( 'wp_new_user_notification' ) ) {
	function wp_new_user_notification( $user ) {
		return;
	}
}


//Add callback function for looping scripts for sending email to admin
add_action( 'wp_enqueue_scripts', 'os_submit_scripts' );

//Register callback function to be called when ajax action 'sendmail_download' is triggered.
add_action( 'wp_ajax_ajax-sendmail_download', 'os_ajax_sendmail_download' );

//Register callback function to be called when ajax action 'sendmail_download' is triggered.
// (FOr users who have not logged in)
add_action( 'wp_ajax_nopriv_ajax-sendmail_download', 'os_ajax_sendmail_download' );


/**
 *  Callback funciton to load the ajax script.
 *
 * @return NULL
 */
function os_submit_scripts() {
	wp_enqueue_script( 'inputtitle_submit',  plugins_url( '/js/os.js', __FILE__ ), array( 'jquery' ) );
	wp_localize_script( 'inputtitle_submit', 'PT_Ajax', array(
		'ajaxurl'   => admin_url( 'admin-ajax.php' ),
		)
	);
}

/**
 * Callback funciton when ajax action 'sendmail_download' is triggered.
 *
 *  THis function will be called when the user clicks on the download link.
 * @return NULL
 */
function os_ajax_sendmail_download() {

	//get the logged in users detials.
	$current_user = wp_get_current_user();

	//Retrieve the admin's email id to send the email.
	$admin_email = get_option( 'admin_email' );

	//Load all values for sending the email.
	$title = $_POST['title'];
	$download_url = $_POST['download-url'];

	//Generate the subject and Message strings - the current user and download link
	$subject = 'User ' . $current_user->user_login . ' has downloaded ' . $download_url ;
	$message = 'Details below :

	User : ' . $current_user->user_login . ' (' . $current_user->user_email . ')
	Downloaded Link : ' . $download_url . '
	Name  :' . $title . '
	DateTime : ' . date( 'l jS \of F Y h:i:s A' ) . '
						   ';
	;

	//Create header formatt as required.
	$headers = 'From: contact@openspecimen.org' . "\r\n" .
	'Reply-To: contact@openspecimen.org' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();

	//Send the email to admin email
	mail( $admin_email, $subject, $message, $headers );

	//Load array to insert in Contact Form DB plugin
	$data = (object) array(
		'title' => 'Downloads By User',
		'posted_data' => array(
			'upload1' => $download_url,
			'email' => $current_user->user_email,
			),
		'uploaded_files' => array( 'fileupload' => $download_url ),
		);

	//Insert the data array into the plugin db.
	do_action_ref_array( 'cfdb_submit', array( &$data ) );
	wp_die(); // this is required to terminate immediately and return a proper response
}
