(function ($) {
	$(document).ready(function () {
		$('#download-btn').click(function () {
			$.post(
				PT_Ajax.ajaxurl,
				{
						// wp ajax action
						action: 'ajax-sendmail_download',
						// vars to send to php funciton
						'download-url': this.getAttribute("href"),
						'title': $('.entry-header').find(".entry-title").text()
					},
					function (response) {
						//console.log(response);
					}
					);
			//return false;
		});
	});
})(jQuery);
